import os
import yaml
import torch
import grpc
import logging
import math
import threading
import time

from core.denoiser import Denoiser
from core import utils
from core.glow import WaveGlow

import tts_pb2

from tts_pb2_grpc import VocoderServicer

MAX_WAV_VALUE = 32768.0
logger = logging.getLogger('waveglow')


class WaveglowServer(VocoderServicer):
    def __init__(self, model_filename, device, sigma, denoiser_strength, volume,
                 mel_chunk_size, is_fp16, crossfade_size, config_path):
        self.lock = threading.Lock()
        self.running_set = set()
        self.waiting_queue = []

        self.config_path = config_path
        if config_path != "":
            if os.path.exists(config_path):
                logger.info('Load Config File: {}'.format(config_path))
                with open(config_path, 'r', encoding='utf-8') as f:
                    config = yaml.load(f, Loader=yaml.FullLoader)
                    model_filename = config['path']
                    sigma = config['sigma']
                    denoiser_strength = config['denoiser_strength']
                    volume = config['volume']
            else:
                self._save_config(model_filename, sigma, denoiser_strength, volume)
        self.model_status = tts_pb2.ModelStatus()

        torch.cuda.set_device(device)
        self.device = device
        self.is_fp16 = is_fp16
        self.crossfade_size = crossfade_size
        self.mel_chunk_size = mel_chunk_size
        self.waveglow_config = None
        self.denoiser = None

        self.left_filter = torch.linspace(start=1.0, end=0.0, steps=self.crossfade_size).cuda()
        self.right_filter = torch.linspace(start=0.0, end=1.0, steps=self.crossfade_size).cuda()

        self.min_wav_value = -MAX_WAV_VALUE
        self.max_wav_value = MAX_WAV_VALUE - 1

        try:
            self._load_checkpoint(model_filename, sigma, denoiser_strength, volume)
        except Exception as e:
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            logger.exception(e)

    @utils.log_error
    def Mel2Wav(self, in_mel, context):
        torch.cuda.set_device(self.device)
        try:
            result = self.SetModel(in_mel.model, context, True)
            if not result.result:
                context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                context.set_details(result.error)
                return tts_pb2.WavData()
            if self.model_status.state != tts_pb2.MODEL_STATE_RUNNING:
                context.set_code(grpc.StatusCode.UNAVAILABLE)
                context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
                return tts_pb2.WavData()

            with torch.no_grad():
                if self.is_fp16:
                    mel_total = torch.HalfTensor([d for d in in_mel.data])
                else:
                    mel_total = torch.FloatTensor([d for d in in_mel.data])
                mel_total = mel_total.view(1, self.mel_config.n_mel_channels, -1)

                previous_audio = None
                for start_idx in range(0, mel_total.size(-1), self.mel_chunk_size):
                    mel = mel_total[:, :, start_idx:start_idx+self.mel_chunk_size]
                    audio, previous_audio = self._mel2wav(mel.cuda(), previous_audio)
                    yield tts_pb2.WavData(data=audio)
        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    @utils.log_error
    def StreamMel2Wav(self, in_mel_iterator, context):
        torch.cuda.set_device(self.device)
        try:
            previous_audio = None
            is_first = True
            with torch.no_grad():
                for in_mel in in_mel_iterator:
                    if is_first:
                        result = self.SetModel(in_mel.model, context, True)
                        if not result.result:
                            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
                            context.set_details(result.error)
                            return tts_pb2.WavData()
                        if self.model_status.state != tts_pb2.MODEL_STATE_RUNNING:
                            context.set_code(grpc.StatusCode.UNAVAILABLE)
                            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
                            return tts_pb2.WavData()
                        is_first = False

                    if self.is_fp16:
                        mel = torch.HalfTensor([d for d in in_mel.data]).cuda()
                    else:
                        mel = torch.FloatTensor([d for d in in_mel.data]).cuda()
                    mel = mel.view(1, self.mel_config.n_mel_channels, -1)
                    audio, previous_audio = self._mel2wav(mel, previous_audio)
                    yield tts_pb2.WavData(data=audio)
        finally:
            with self.lock:
                self.running_set.discard(threading.get_ident())

    def _mel2wav(self, mel, previous_audio):
        audio = self.waveglow.infer(mel, self.sigma)
        if self.denoiser_strength > 0:
            pad_size = self.waveglow.upsample.stride[0] * 3 - audio.size(1)
            if 0 < pad_size:
                audio = torch.nn.functional.pad(audio, (0, pad_size), 'constant')
            audio = self.denoiser(audio, self.denoiser_strength)
            if 0 < pad_size:
                audio = audio[:, :, :-pad_size]
        else:
            audio = audio.float()
        audio = MAX_WAV_VALUE * audio.squeeze() * self.volume
        if previous_audio is not None:
            audio[:self.crossfade_size] = previous_audio * self.left_filter + self.right_filter * audio[:self.crossfade_size]
        previous_audio = audio[-self.crossfade_size:]
        audio = audio[:-self.crossfade_size]
        audio = torch.clamp(audio,
                            min=self.min_wav_value, max=self.max_wav_value)
        audio = audio.short().tolist()
        return audio, previous_audio

    @utils.log_error
    def GetMelConfig(self, empty, context):
        if self.model_status.state == tts_pb2.MODEL_STATE_RUNNING:
            return self.mel_config
        else:
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            context.set_details(tts_pb2.ModelState.Name(self.model_status.state))
            return tts_pb2.MelConfig()

    @utils.log_error
    def SetModel(self, in_model, context, is_running=False):
        result = None
        waiting_key = None
        with self.lock:
            if 0 < len(self.waiting_queue):
                waiting_key = threading.get_ident()
                self.waiting_queue.append(waiting_key)
            else:
                if in_model is None or in_model.path == "" or in_model == self.model_status.model:
                    if is_running:
                        self.running_set.update([threading.get_ident()])
                    result = tts_pb2.SetModelResult(result=True)
                elif in_model != self.model_status.model:
                    if 0 < len(self.running_set):
                        waiting_key = threading.get_ident()
                        self.waiting_queue.append(waiting_key)
                    else:
                        if is_running:
                            self.running_set.update([threading.get_ident()])
                        result = self._set_model(in_model)

        if waiting_key is not None:
            while True:
                with self.lock:
                    if self.waiting_queue[0] == waiting_key:
                        if in_model == self.model_status.model or in_model.path == "":
                            if is_running:
                                self.running_set.update([threading.get_ident()])
                            del self.waiting_queue[0]
                            result = tts_pb2.SetModelResult(result=True)
                            break
                        elif in_model != self.model_status.model:
                            if 0 == len(self.running_set):
                                if is_running:
                                    self.running_set.update([threading.get_ident()])
                                del self.waiting_queue[0]
                                result = self._set_model(in_model)
                                break
                time.sleep(0.01)
        return result

    @utils.log_error
    def GetModel(self, empty, context):
        return self.model_status

    def _set_model(self, in_model):
        logger.debug('_set_model/in_model/%s', in_model)
        try:
            if not os.path.exists(in_model.path):
                raise RuntimeError("Model {} does not exist.".format(in_model.path))
            sigma = float(in_model.config["sigma"]) if "sigma" in in_model.config else 0.666
            denoiser_strength = float(in_model.config["denoiser_strength"]) if "denoiser_strength" in in_model.config else 0.01
            volume = float(in_model.config["volume"]) if "volume" in in_model.config else 1.0
        except Exception as e:
            logger.exception(e)
            return tts_pb2.SetModelResult(result=False, error=str(e))

        try:
            self._load_checkpoint(in_model.path, sigma, denoiser_strength, volume)
            self._save_config(in_model.path, sigma, denoiser_strength, volume)
            return tts_pb2.SetModelResult(result=True)
        except Exception as e:
            logger.exception(e)
            self.model_status.state = tts_pb2.MODEL_STATE_NONE
            self.model_status.model.Clear()
            return tts_pb2.SetModelResult(result=False, error=str(e))

    def _load_checkpoint(self, model_filename, sigma, denoiser_strength, volume):
        self.model_status.state = tts_pb2.MODEL_STATE_LOADING

        self.sigma = sigma
        self.denoiser_strength = denoiser_strength
        self.volume = math.pow(10, 0.5 * math.log2(volume))

        if model_filename != self.model_status.model.path:
            checkpoint = torch.load(model_filename, map_location='cpu')

            if self.waveglow_config != checkpoint['waveglow_config']:
                self.waveglow_config = checkpoint['waveglow_config']
                waveglow = WaveGlow(**checkpoint['waveglow_config']).cuda()

                self.waveglow = waveglow.remove_weightnorm(waveglow)
                self.waveglow.eval()
                if self.is_fp16:
                    from apex import amp
                    self.waveglow, _ = amp.initialize(self.waveglow, [], opt_level="O3")
            else:
                self.waveglow.remove_W_inverse(self.waveglow)
            self.waveglow.load_state_dict(checkpoint['inference_model'])

            data_config = checkpoint['data_config']
            self.mel_config = tts_pb2.MelConfig(
                filter_length=data_config['filter_length'],
                hop_length=data_config['hop_length'],
                win_length=data_config['win_length'],
                n_mel_channels=self.waveglow.upsample.in_channels,
                sampling_rate=data_config['sampling_rate'],
                mel_fmin=data_config['mel_fmin'],
                mel_fmax=data_config['mel_fmax']
            )
            if denoiser_strength > 0:
                if self.denoiser is None or self.denoiser.n_mel_channels != self.mel_config.n_mel_channels:
                    self.denoiser = Denoiser(self.waveglow, self.mel_config.n_mel_channels).cuda()
                else:
                    self.denoiser.load(self.waveglow)
            torch.cuda.empty_cache()

            self.model_status.model.path = model_filename
        self.model_status.model.config["sigma"] = str(sigma)
        self.model_status.model.config["denoiser_strength"] = str(denoiser_strength)
        self.model_status.model.config["volume"] = str(volume)

        self.model_status.state = tts_pb2.MODEL_STATE_RUNNING

    def _save_config(self, model_filename, sigma, denoiser_strength, volume):
        if self.config_path != "":
            with open(self.config_path, 'w', encoding='utf-8') as wf:
                config = {
                    'path': model_filename,
                    'sigma': sigma,
                    'denoiser_strength': denoiser_strength,
                    'volume': volume
                }
                yaml.dump(config, wf, default_flow_style=False)
                logger.debug('Save Config File: {}'.format(self.config_path))
