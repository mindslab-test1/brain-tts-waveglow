import argparse

from client import VocoderClient

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='127.0.0.1:35001')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        help='Model Path.',
                        type=str,
                        default="")
    parser.add_argument('-s', '--sigma',
                        nargs='?',
                        dest='sigma',
                        help='sigma',
                        type=float,
                        default=-1)
    parser.add_argument('-ds', '--denoiser_strength',
                        nargs='?',
                        dest='denoiser_strength',
                        help='denoiser strength',
                        type=float,
                        default=-1)
    parser.add_argument('-v', '--volume',
                        nargs='?',
                        dest='volume',
                        help='audio volume',
                        type=float,
                        default=-1)

    args = parser.parse_args()

    client = VocoderClient(args.remote)
    model_status = client.get_model()

    print('before model_status: {}'.format(model_status))
    if args.model == "":
        model = model_status.model.path
    else:
        model = args.model

    if args.sigma == -1:
        sigma = model_status.model.config['sigma']
    else:
        sigma = args.sigma

    if args.denoiser_strength == -1:
        denoiser_strength = model_status.model.config['denoiser_strength']
    else:
        denoiser_strength = args.denoiser_strength

    if args.volume == -1:
        volume = model_status.model.config['volume']
    else:
        volume = args.volume

    client.set_model(
        model, sigma, denoiser_strength, volume
    )

    model_status = client.get_model()
    print('after model_status: {}'.format(model_status))
