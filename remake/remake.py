import torch
import argparse

from maum.brain.tts.waveglow.core.glow import WaveGlow

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='waveglow runner executor')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        dest='input',
                        required=True,
                        help='input Model Path.',
                        type=str)
    parser.add_argument('-o', '--output',
                        nargs='?',
                        dest='output',
                        required=True,
                        help='output Model Path.',
                        type=str)
    args = parser.parse_args()

    checkpoint = torch.load(args.input, map_location='cpu')

    if 'waveglow_config' in checkpoint:
        waveglow_config = checkpoint['waveglow_config']
        waveglow = WaveGlow(**waveglow_config)
        waveglow.load_state_dict(checkpoint['model'])
        waveglow.remove_weightnorm(waveglow)

        checkpoint['inference_model'] = waveglow.state_dict()
    else:
        waveglow = checkpoint['model']
        checkpoint['model'] = waveglow.state_dict()

        waveglow = waveglow.remove_weightnorm(waveglow)
        checkpoint['inference_model'] = waveglow.state_dict()

        checkpoint['waveglow_config'] = {
            "n_mel_channels": 80,
            "n_flows": 12,
            "n_group": 8,
            "n_early_every": 4,
            "n_early_size": 2,
            "WN_config": {
                "n_layers": 8,
                "n_channels": 256,
                "kernel_size": 3
            }
        }

    torch.save(checkpoint, args.output)
