#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import time
import grpc
import argparse
import signal

from concurrent import futures
from grpc_health.v1 import health
from grpc_health.v1 import health_pb2_grpc

from run.runner import WaveglowServer
from tts_pb2_grpc import add_VocoderServicer_to_server
from core.utils import set_logger

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='waveglow runner executor')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Path.',
                        type=str)
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=35001)
    parser.add_argument('-d', '--device',
                        nargs='?',
                        dest='device',
                        help='gpu device',
                        type=int,
                        default=0)
    parser.add_argument('-s', '--sigma',
                        nargs='?',
                        dest='sigma',
                        help='sigma',
                        type=float,
                        default=0.666)
    parser.add_argument('-ds', '--denoiser_strength',
                        nargs='?',
                        dest='denoiser_strength',
                        help='denoiser strength',
                        type=float,
                        default=0.01)
    parser.add_argument('-v', '--volume',
                        nargs='?',
                        dest='volume',
                        help='audio volume',
                        type=float,
                        default=1.0)
    parser.add_argument('-w', '--max_workers',
                        nargs='?',
                        dest='max_workers',
                        help='max workers',
                        type=int,
                        default=6)
    parser.add_argument('--crossfade_size',
                        nargs='?',
                        dest='crossfade_size',
                        help='crossfade size',
                        type=int,
                        default=200)
    parser.add_argument('--mel_chunk_size',
                        nargs='?',
                        dest='mel_chunk_size',
                        help='mel chunk size',
                        type=int,
                        default=88)
    parser.add_argument('--is_fp16', action='store_true',
                        help='fp16 mode')
    parser.add_argument('-c', '--config',
                        nargs='?',
                        dest='config',
                        help='yaml file for configuration',
                        type=str,
                        default="")

    args = parser.parse_args()

    logger = set_logger(args.log_level)

    waveglow = WaveglowServer(
        args.model, args.device, args.sigma, args.denoiser_strength, args.volume,
        args.mel_chunk_size, args.is_fp16, args.crossfade_size, args.config)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers), )
    add_VocoderServicer_to_server(waveglow, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    health_servicer = health.HealthServicer()
    health_pb2_grpc.add_HealthServicer_to_server(health_servicer, server)
    server.start()

    def exit_gracefully(signum, frame):
        health_servicer.enter_graceful_shutdown()
        server.stop(60)
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    logger.info('waveglow starting at 0.0.0.0:%d', args.port)

    server.wait_for_termination()
