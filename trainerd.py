import argparse
import json
import torch

from train.train import train


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output_directory', type=str, default="trained",
                        help='directory to save checkpoints')
    parser.add_argument('-l', '--log_directory', type=str, default="logs/tensorboard",
                        help='directory to save tensorboard logs')
    parser.add_argument('-c', '--config', type=str, default="train/config.json",
                        help='JSON file for configuration')
    parser.add_argument('-r', '--rank', type=int, default=0,
                        help='rank of process for distributed')
    parser.add_argument('--local_rank', type=int, default=0,
                        help='local rank of current gpu')
    parser.add_argument('--num_gpus', type=int, default=1,
                        help='number of gpus')
    parser.add_argument('-g', '--group_name', type=str, default='',
                        help='name of group for distributed')
    args = parser.parse_args()

    # Parse configs.  Globals nicer in this case
    with open(args.config) as f:
        data = f.read()
    config = json.loads(data)
    train_config = config["train_config"]
    train_data_config = config["data_config"]
    val_data_config = train_data_config.copy()
    val_data_config['training_files'] = config["validation_files"]
    val_data_config['segment_length'] = train_data_config['segment_length'] * train_config['batch_size']
    dist_config = config["dist_config"]
    waveglow_config = config["waveglow_config"]

    torch.backends.cudnn.enabled = True
    torch.backends.cudnn.deterministic = False
    torch.backends.cudnn.benchmark = True

    train_config["output_directory"] = args.output_directory
    train_config["log_directory"] = args.log_directory

    group_name = 'group_{}'.format(args.group_name)

    train(dist_config, waveglow_config, train_data_config, val_data_config,
          args.num_gpus, args.rank, args.local_rank, args.group_name, **train_config)
