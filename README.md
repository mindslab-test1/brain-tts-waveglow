# brain-tts-waveglow

## Setup
1. `pip install -r requirements.txt`
1. `python -m grpc.tools.protoc --proto_path=tts/proto/src/main/proto/maum/brain/tts --python_out=. --grpc_python_out=. tts/proto/src/main/proto/maum/brain/tts/tts.proto`

## Train
1. `python trainerd.py --output_directory=outdir --log_directory=logdir -c=config.json`

## Distributed Train
1. `python distributed.py --output_directory=outdir --log_directory=logdir -c=config.json`

## Run Server
1. `python server.py -m model.pt`

## Run Client
1. `python client.py`
