import random

from tensorboardX import SummaryWriter

from .plotting_utils import plot_waveform_to_numpy


class WaveglowLogger(SummaryWriter):
    def __init__(self, logdir, sample_rate):
        super(WaveglowLogger, self).__init__(logdir)
        self.sample_rate = sample_rate
        self.is_first = True

    def log_training(self, reduced_loss, learning_rate, duration,
                     iteration):
        self.add_scalar("training.loss", reduced_loss, iteration)
        self.add_scalar("learning.rate", learning_rate, iteration)
        self.add_scalar("duration", duration, iteration)

    def log_validation(self, reduced_loss, model, y, y_pred, iteration):
        self.add_scalar("validation.loss", reduced_loss, iteration)

        # plot distribution of parameters
        for tag, value in model.named_parameters():
            tag = tag.replace('.', '/')
            try:
                self.add_histogram(tag, value.data.cpu().numpy(), iteration)
            except Exception as e:
                print(tag, value.data.cpu().numpy())
                raise e

        # plot alignment, mel target and predicted, gate target and predicted
        idx = random.randint(0, y.size(0) - 1)

        audio_pred = y_pred[idx]
        audio_target = y[idx]

        self.add_audio(
            "raw_audio_predicted",
            audio_pred,
            iteration,
            self.sample_rate
        )
        self.add_image(
            "waveform_predicted",
            plot_waveform_to_numpy(audio_pred.data.cpu().numpy()),
            iteration
        )
        if self.is_first:
            self.add_audio(
                "raw_audio_target",
                audio_target,
                iteration,
                self.sample_rate
            )
            self.add_image(
                "waveform_target",
                plot_waveform_to_numpy(audio_target.data.cpu().numpy()),
                iteration
            )
        self.is_first = False
