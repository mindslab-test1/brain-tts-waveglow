import time
import logging
import grpc
import os
logger = logging.getLogger('waveglow')


def set_logger(level):
    os.environ['TZ'] = 'Asia/Seoul'
    try:
        time.tzset()
    except AttributeError as e:
        print(e)
        print("Skipping timezone setting.")
    custom_logger(name='waveglow')
    logger = logging.getLogger('waveglow')
    if level == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif level == 'INFO':
        logger.setLevel(logging.INFO)
    elif level == 'WARNING':
        logger.setLevel(logging.WARNING)
    elif level == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif level == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    return logger


def custom_logger(name):
    fmt = '[{}|%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s'.format(name)
    fmt_date = '%Y-%m-%d_%T %Z'

    handler = logging.StreamHandler()

    formatter = logging.Formatter(fmt, fmt_date)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)


def log_error(func):
    def wrapper(*args, **kwargs):
        try:
            logger.debug('start %s', func)
            return func(*args, **kwargs)
        except Exception as e:
            logger.exception(e)
            context = args[2]
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))
        finally:
            logger.debug('end %s', func)
    return wrapper
