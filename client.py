#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import grpc
import google.protobuf.empty_pb2 as empty

from tts_pb2_grpc import VocoderStub
from tts_pb2 import MelSpectrogram, Model


class VocoderClient(object):
    def __init__(self, remote='127.0.0.1:35001'):
        channel = grpc.insecure_channel(remote)
        self.stub = VocoderStub(channel)

    def mel2wav(self, mel):
        return self.stub.Mel2Wav(mel)

    def get_mel_config(self):
        return self.stub.GetMelConfig(empty.Empty())

    def set_model(self, checkpoint_path, sigma=0.666, denoiser_strength=0.01, volume=1.0):
        in_model = Model(
            path=checkpoint_path,
            config={
                "sigma": str(sigma),
                "denoiser_strength": str(denoiser_strength),
                "volume": str(volume)
            }
        )
        return self.stub.SetModel(in_model)

    def get_model(self):
        return self.stub.GetModel(empty.Empty())


if __name__ == '__main__':
    client = VocoderClient()

    mel_config = client.get_mel_config()
    print(mel_config)

    wav_data = client.mel2wav(
        MelSpectrogram(data=[i/10 for i in range(80*80)])
    )
    for data in wav_data:
        print(data)

    model = client.get_model()
    print(model)
